# Contributor: Thomas Boerger <thomas@webhippie.de>
# Contributor: Gennady Feldman <gena01@gmail.com>
# Contributor: Sergii Sadovyi <serg.sadovoi@gmail.com>
# Contributor: Galen Abell <galen@galenabell.com>
# Maintainer: Thomas Boerger <thomas@webhippie.de>
pkgname=terraform
pkgver=1.2.7
pkgrel=0
pkgdesc="Building, changing and combining infrastructure safely and efficiently"
url="https://www.terraform.io/"
arch="all"
license="MPL-2.0"
makedepends="go"
checkdepends="openssh-client"
source="$pkgname-$pkgver.tar.gz::https://github.com/hashicorp/terraform/archive/refs/tags/v$pkgver.tar.gz"
options="net"

export GOFLAGS="$GOFLAGS -modcacherw -mod=readonly -trimpath"
export GOCACHE="$srcdir/go-cache"
export GOTMPDIR="$srcdir"
export GOMODCACHE="$srcdir/go"

build() {
	go build -v -o bin/$pkgname \
		-ldflags "-X main.GitCommit=v$pkgver -X github.com/hashicorp/terraform/version.Prerelease="
}

check() {
	case "$CARCH" in
	arm*|x86)
		go list . | xargs -t -n4 \
			go test -timeout=2m -parallel=4
		;;
	*)
		go test ./...
		;;
	esac
	bin/$pkgname -v
}

package() {
	install -Dm755 "$builddir"/bin/$pkgname -t "$pkgdir"/usr/bin
}

sha512sums="
d77db9040bd19f4a6e2988a61ecb7dc5adecf46f798d90e1232f9ffe385668ecb0869f1877d5f6ea8a36d48e1e47fffa463b9809046b524af741ddb5b67b469e  terraform-1.2.7.tar.gz
"
